@extends('layout.main') {{--  uses the layout in layout/main --}}

@section('content') {{-- puts all content from here until the @stop --}}
  @if(Auth::check())
    <p>Hello, {{ Auth::user()->username }}.</p>
  @else
    <p>You are not signed in.</p>
  @endif
@stop