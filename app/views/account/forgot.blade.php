@extends('layout.main')

@section('content')

  <div class="row">
    <form class="form-horizontal col-sm-6 col-sm-offset-3" role="form" action="{{ URL::route('account-forgot-password-post') }}" method="post" novalidate>


      <!-- start: email -->
      <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email"  {{ (Input::old('email')) ? ' value="' . e(Input::old('email')) . '"' : '' }}>
          @if($errors->has('email'))
            {{ $errors->first('email') }}
          @endif
        </div>
      </div>
      <!-- end: email -->

      <!-- start: submit -->
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Recover</button>
          {{ Form::token() }}
        </div>
      </div>
      <!-- end: submit -->
  
    </form>
  </div>

@stop
