@extends('layout.main')

@section('content')

  <div class="row">
    <form class="form-horizontal col-sm-6 col-sm-offset-3" role="form" action="{{ URL::route('account-change-password-post') }}" method="post" novalidate>

      <!-- start: old password -->
      <div class="form-group">
        <label for="old_password" class="col-sm-2 control-label">Old Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password">
          @if($errors->has('old_password'))
            {{ $errors->first('old_password') }}
          @endif
        </div>
      </div>
      <!-- end: old password -->

      <!-- start: new password -->
      <div class="form-group">
        <label for="password" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          @if($errors->has('password'))
            {{ $errors->first('password') }}
          @endif
        </div>
      </div>
      <!-- end: new password -->

      <!-- start: password again -->
      <div class="form-group">
        <label for="password_again" class="col-sm-2 control-label">Password Again</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="password_again" name="password_again" placeholder="Password">
          @if($errors->has('password_again'))
            {{ $errors->first('password_again') }}
          @endif
        </div>
      </div>
      <!-- end: password again -->

      <!-- start: submit -->
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Change password</button>
          {{ Form::token() }}
        </div>
      </div>
      <!-- end: submit -->

    </form>
  </div>   

@stop