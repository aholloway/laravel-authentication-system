@extends('layout.main')

@section('content')

  <div class="row">
    <form class="form-horizontal col-sm-6 col-sm-offset-3" role="form" action="{{ URL::route('account-sign-in-post') }}" method="post" novalidate>

      <!-- start: email -->
      <div class="form-group">
        <label for="email" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email" {{ Input::old('email') ? ' value="' . Input::old('email') .'"' : '' }}>
          @if($errors->has('email'))
            {{ $errors->first('email') }}
          @endif
        </div>
      </div>
      <!-- end: email -->

    <!-- start: password -->
      <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Password</label>
        <div class="col-sm-9">
          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
          @if($errors->has('password'))
            {{ $errors->first('password') }}
          @endif
        </div>
      </div>
      <!-- end: password -->

      <!-- start: remember -->
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember" id="remember"> Remember me
            </label>
          </div>
        </div>
      </div>
      <!-- end: remember -->

      <!-- start: submit -->
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-default">Sign in</button>
          {{ Form::token() }}
        </div>
      </div>
      <!-- end: submit -->

    </form>
  </div>

@stop