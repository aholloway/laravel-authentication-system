@extends('layout.main')

@section('content')
  
  <!-- Output all errors once form is submitted
    <pre>{{ print_r($errors) }}</pre>
  -->

  <div class="row">
    <form class="form-horizontal col-sm-6 col-sm-offset-3" role="form" action="{{ URL::route('account-create-post') }}" method="post" novalidate>

      <!-- start: email -->
      <div class="form-group">
        <label for="email" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
          <input type="email" name="email" id="email" class="form-control" placeholder="Email"  {{ Input::old('email') ? ' value="' . e(Input::old('email')) . '"' : '' }}>
          @if($errors->has('email'))
            {{ $errors->first('email') }}
          @endif
        </div>
      </div>
      <!-- end: email -->

      <!-- start: username -->
      <div class="form-group">
        <label for="username" class="col-sm-3 control-label">Username</label>
        <div class="col-sm-9">
          <input type="text" name="username" id="username" class="form-control" placeholder="Username"  {{ Input::old('username') ? ' value="' . e(Input::old('username')) . '"' : '' }}>
          @if($errors->has('username'))
            {{ $errors->first('username') }}
          @endif
        </div>
      </div>
      <!-- end: username -->

      <!-- start: password -->
        <div class="form-group">
          <label for="password" class="col-sm-3 control-label">Password</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            @if($errors->has('password'))
              {{ $errors->first('password') }}
            @endif
          </div>
        </div>
        <!-- end: password -->

        <!-- start: password again -->
        <div class="form-group">
          <label for="password_again" class="col-sm-3 control-label">Password Again</label>
          <div class="col-sm-9">
            <input type="password" class="form-control" id="password_again" name="password_again" placeholder="Password Again">
            @if($errors->has('password_again'))
              {{ $errors->first('password_again') }}
            @endif
          </div>
        </div>
        <!-- end: password again -->

        <!-- start: submit -->
        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-default">Create account</button>
            {{ Form::token() }}
          </div>
        </div>
        <!-- start: submit -->

    </form>
  </div>

@stop