<!DOCTYPE html>
<html dir="ltr" lang="en-GB">
  <head>
    <meta charset="UTF-8" />
    <title>Authentication system</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
  </head>
  <body>

    @include('layout.navigation')

    <div class="container">

      @if(Session::has('global'))
        <p>{{ Session::get('global') }}</p>
      @endif

      @yield('content')
    </div><!-- /.container -->
  </body>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</html>