<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
  'as'    => 'home', // defines the name of the URL on the line above as `home`
  'uses'  => 'HomeController@home' // defines which controller it uses. `Controller@method`
));

// User profile - accessable by anyone
Route::get('/user/{username}', array( // {username} gets what is passed through URL
  'as'   => 'profile-user', // defines the name of the URL on the line above as `profile-user`
  'uses' => 'ProfileController@user' // defines which controller it uses. `Controller@method`
));

/*
/ Authenticated group
*/
Route::group(array('before' => 'auth'), function() {

  /*
  / CSRF protection group
  */
  Route::group(array('before' => 'csrf'), function() {

      /*
      / Change password (POST)
      */
      Route::post('/account/change-password', array(
        'as'   => 'account-change-password-post', // shortname given for the URL above
        'uses' => 'AccountController@postChangePassword' // uses the Controller@method
      ));

  });

  /*
  / Change password (GET)
  */
  Route::get('/account/change-password', array(
    'as'    => 'account-change-password', // shortname given for the URL above
    'uses'  => 'AccountController@getChangePassword' // uses the Controller@method
  ));

  /*
  / Sign out (GET)
  */
  Route::get('/account/sign-out/', array(
    'as'   => 'account-sign-out', // shortname given for the URL above
    'uses' => 'AccountController@getSignOut' // uses the Controller@method
  ));

});

/*
/ Unauthenticated group
*/
Route::group(array('before' => 'guest'), function() { // before access to any routes is granted. Guest is a filter set in config/filters.php

  /*
  / CSRF protection group
  */
  Route::group(array('before' => 'csrf'), function() { // CSRF filter used

    /*
    / Create account (POST)
    */
    Route::post('/account/create', array( // URL
      'as'    => 'account-create-post', // shortname given for the URL above
      'uses'  => 'AccountController@postCreate' // uses the Controller@method
    ));

    /*
    / Sign in (POST)
    */
    Route::post('/account/sign-in', array( // URL
      'as'   => 'account-sign-in-post', // shortname given for the URL above
      'uses' => 'AccountController@postSignIn' // uses the Controller@method
    ));

    /*
    / Forgot password (POST)
    */
    Route::post('/account/forgot', array(
      'as'   => 'account-forgot-password-post',
      'uses' => 'AccountController@postForgotPassword'
    ));

  });

  /*
  / Forgot password (GET)
  */
  Route::get('/account/forgot-password', array(
    'as'   => 'account-forgot-password',
    'uses' => 'AccountController@getForgotPassword'
  ));

  Route::get('/account/recover/{code}', array(
    'as'   => 'account-recover',
    'uses' => 'AccountController@getRecover'
  ));

  /*
  / Sign in (GET)
  */
  Route::get('/account/sign-in', array(
    'as'   => 'account-sign-in',
    'uses' => 'AccountController@getSignIn'
  ));

  /*
  / Create account (GET)
  */
  Route::get('/account/create', array( // URL
    'as'    => 'account-create', // shortname given for the URL above
    'uses'  => 'AccountController@getCreate' // uses the Controller@method
  ));

  Route::get('/account/activate/{code}', array(
    'as'    => 'account-activate',
    'uses'  => 'AccountController@getActivate'
  ));

});