<?php

class HomeController extends BaseController {
	public function home() {

		// echo $user = User::find(1)->username; // using the `User` model to find a user (from the user table) with the ID of
		
		/*
		Mail::send('emails.auth.test', array('name' => 'Adam'), function($message) { // folderName/folderName/fileName
			$message->to('adam.holloway21@gmail.com', 'Adam Holloway')->subject('Test email');
		});
		*/

		return View::make('home'); // returns content of the `home` view which is `home.blade.php`
	}
}