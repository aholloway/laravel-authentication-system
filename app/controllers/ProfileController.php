<?php
class ProfileController extends BaseController {
  public function user($username) {
    $user = User::where('username', '=', $username); // gets username from URL and assigns to variable

    if($user->count()) { // if the username is found
      $user = $user->first(); // returns first record from the query
      return  View::make('profile.user') // return their profile
              ->with('user', $user); // allows object information to be used in the view
    }

    return App::abort(404);

  }
}