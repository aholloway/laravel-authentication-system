<?php
class AccountController extends BaseController {

  public function getSignIn() {
    return View::make('account.signin');
  }

  // Sign in
  public function postSignIn() {
    $validator = Validator::make(Input::all(),
      array(
        'email'     => 'required|email',
        'password'  => 'required'
      )
    );

    // If sign in validation fails
    if($validator->fails()) {
      return  Redirect::route('account-sign-in')
              ->withErrors($validator) // show errors
              ->withInput(); // keep the old input
    } else {

      // checks if an input of `remeber` is available and assign to variable true/false value
      $remember = (Input::has('remember')) ? true : false;

      // Attempt authorisation
      $auth = Auth::attempt(array(
        'email'    => Input::get('email'),
        'password' => Input::get('password'),
        'active'   => 1
      ), $remember);

      if($auth) {
        // Redirect to the intended page (page which user intended to access but was faced with login before they are granted access)
        return Redirect::intended('/');
      } else {
        return  Redirect::route('account-sign-in')
                ->with('global', 'Email/password wrong, or account not activated.');
      }

    }

    // if username/password is incorrect
    return  Redirect::route('account-sign-in')
            ->with('global', 'There was a problem signing you in. Have you activated?');

  }

  public function getSignOut() {
    Auth::logout(); // signs user out
    return Redirect::route('home'); // redirect to homepage
  }

  // Creating an account
  public function getCreate() {
    return View::make('account.create');
  }

  public function postCreate() {

    // print_r(Input::all()); // print all data submitted in the form to the screen

    $validator = Validator::make(Input::all(), // get all inputted date in form
      array(
        'email'           => 'required|max:50|email|unique:users', // validation rules
        'username'        => 'required|max:20|min:3|unique:users', // unique:users = unique in `users` table
        'password'        => 'required|min:6',
        'password_again'  => 'required|same:password' // same:password = field must match `password` field
      )
    );

    // check if validation has failed
    if($validator->fails()) {
      return  Redirect::route('account-create') // return redirect to a route
              ->withErrors($validator) // takes all errors from $validator and passes it to the route/URL `account-create`
              ->withInput(); // allows access to the input so that input can be put back into the form
    } else {

      $email    = Input::get('email');
      $username = Input::get('username');
      $password = Input::get('password');

      // Activation code
      $code = str_random(60);

      $user = User::create(array( // creating a method called `create` within the user model. Method is not actually in the /model/user.php file
        'email'     => $email,
        'username'  => $username,
        'password'  => Hash::make($password), // hashes the password
        'code'      => $code,
        'active'    => 0
      ));

      // If user is create
      if($user) {

        // Send message
        Mail::send('emails.auth.activate', array('link' => URL::route('account-activate', $code),'username' => $username), function($message) use ($user) {
            $message->to($user->email, $user->username)->subject('Activate your account');
        });

        return  Redirect::route('home')
                ->with('global', 'Your account has been created! We have sent you and email to active your account' ); // `global` is a new functionality in main.blade.php
      }
    }
  }

  // This function is for when the user clicks the account activation link via the email
  public function getActivate($code) {
    $user = User::where('code', '=', $code)->where('active', '=', 0);

    if($user->count()) {
      $user = $user->first(); // $user = first record in database

      // Update user to active state
      $user->active = 1; // sets `active` column in database to 1
      $user->code = ''; // replaces code in `code` column in database with an empty string

      // checks if `active` and `code` column data has been saved to database
      if($user->save()) {
        return  Redirect::route('home')
                ->with('global', 'Activated! You can now sign in!'); // global is flash data
      }
    }

    // if save is unsuccessful
    return  Redirect::route('home')
            ->with('global', 'We could not activate your account. Try again later.');

  }

  public function getChangePassword() {
    return View::make('account.password');
  }

  public function postChangePassword() {
    $validator = Validator::make(Input::all(),
      array(
        'old_password'   => 'required',
        'password'       => 'required|min:6',
        'password_again' => 'required|same:password'
      )
    );

    if($validator->fails()) {
      return Redirect::route('account-change-password')
             ->withErrors($validator);     
    } else {
      $user         = User::find(Auth::user()->id); // selects current user

      $old_password = Input::get('old_password'); // getting it from the from user input
      $password     = Input::get('password');

      // check if old password matches current password
      if(Hash::check($old_password, $user->getAuthPassword())) {
        $user->password = Hash::make($password); // update row in database with new hashed password

        if($user->save()) { // if data has been saved to database
          return  Redirect::route('home') // redirect to homepage
                  ->with('global', 'Your password has been changed');
        }

      } else { // old password is incorrect
        return  Redirect::route('account-change-password')
                ->with('global', 'Your old password is incorrect'); // global is flash data
      }

    }

    // fallback
    return  Redirect::route('account-change-password')
            ->with('global', 'Your password could not be changed'); // global is flash data
  }

  public function getForgotPassword() {
    return View::make('account.forgot');
  }

  public function postForgotPassword() {
    $validator = Validator::make(Input::all(),
      array( 
        'email' => 'required|email'
      )
    );

    // if validation fails
    if($validator->fails()) {
      return  Redirect::route('account-forgot-password')
              ->withErrors($validator)
              ->withInput();
    } else {
      // change password
      
      $user = User::where('email', '=', Input::get('email'));

      if ($user->count()) { // if count is positive (user exisits)
        $user               = $user->first(); // grabs the user
      }

      // Generate a new code and password
      $code                 = str_random(60);
      $password             = str_random(10);

      // assigning to database record
      $user->code           = $code;
      $user->password_temp  = Hash::make($password);

      if ($user->save()) {
        // send email
        Mail::send('emails.auth.forgot', array('link' => URL::route('account-recover', $code), 'username' => $user->username, 'password' => $password), function($message) use ($user) {
          $message->to($user->email, $user->username)->subject('Your new password');
        });

        // redirect to home telling them we've sent them an email
        return  Redirect::route('home')
                ->with('global', 'We have sent you a new password by email.');

      }

    }

    // fallback
    return  Redirect::route('account-forgot-password')
            ->with('global', 'Could not request new password.');

  }

  public function getRecover($code) {
    $user = User::where('code', '=', $code)
            ->where('password_temp', '!=', '');

    if ($user->count()) {
      $user = $user->first();

      $user->password       = $user->password_temp; // setting temp password as main password
      $user->password_temp  = ''; // set value to empty
      $user->code           = ''; // set value to empty

      if($user->save()) { // if information saves to db
        return  Redirect::route('home')
                ->with('global', 'Your account has been recovered and you can sign in with your new password');
      }
      
    }

    // fallback if above fails
    return  Redirect::route('home')
            ->with('global', 'Could not recover your account');

  }

}