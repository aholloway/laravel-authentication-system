<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) { // creates table `users`
			$table->increments('id'); // assumes it an integer, increments and primary

			$table->string('email', 50); // string = varchar
			$table->string('username', 20);
			$table->string('password', 60);
			$table->string('password_temp', 60);
			$table->string('code', 60);

			$table->integer('active');

			$table->timestamps(); // generates created_at and updated_at rows
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
