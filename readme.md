# Laravel Authentication System

## About
Authentication system with ability to create accounts, sign in, request password, change password and server-side validation for all fields.

## Email
Uses Gmail to send emails. Edit lines 57, 83 and 96 in app/config/mail.php

## Database Migration
Generates database from migration. To generate table/s, open the terminal, change directory into the Laravel folder and run `php artisan migrate`. Note: database will need creating first. See config/database.php for info.

## Requirements
Laravel and Composer.

## Setup
1. Install [Composer](https://getcomposer.org/)
2. Create a [Laravel 4 project](http://laravel.com/docs/quick)
3. Replace the default 'app' folder with this one